# Melody

### *Yet another* responsive OOCSS grid framework

Check the page at <http://melody.molovo.co.uk> to view the features.

Building the grid is as easy as:

    <div class="container">
      <div class="grid 1of1"></div>

      <div class="grid 1of3"></div>
      <div class="grid 1of3"></div>
      <div class="grid 1of3"></div>
    </div>
